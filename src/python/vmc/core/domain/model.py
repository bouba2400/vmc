import uuid
from _functools import reduce

class Error(Exception):
    pass

class InputError(Error):
    pass

class LogicError(Error):
    pass

class ItemDispenser:
    def dispenseItem(self, shelfNumber):
        raise NotImplementedError

class CashDispenser:
    def dispenseCash(self, amount):
        raise NotImplementedError

class Product:
    def __init__(self):
        self.id = None
        self.price = None
        self.code = None
        self.stock = None
    
    def decrementStock(self):
        self.stock = self.stock - 1

class SalesTransaction:
    def __init__(self):
        self.id = None
        self.price = None
        self.shelfNumber = None
        self.cashPayments = []
        self.moment = None
        self.productCode = None
        self.quantity = 1
    
    def updateDetails(self, productCode, price, shelfNumber, cashPayments):
        self.productCode = productCode
        self.shelfNumber = shelfNumber
        self.cashPayments = cashPayments
        self.price = price

class Device:
    def __init__(self, modelFactory, cashDispenser):
        self.modelFactory = modelFactory
        self.shelvesByNumber = None
        self.returnedCashPayments = []
        self.pendingCashPayments = []
        self.salesTransactions = []
        self.shelves = []
        self.cashDispenser = cashDispenser
    
    def getShelvesByNumber(self):
        if not self.shelvesByNumber:
            self.shelvesByNumber = {}
            for x in self.shelves:
                self.shelvesByNumber[x.number] = x
        return self.shelvesByNumber
    
    def acceptCashPayment(self, amount):
        self.pendingCashPayments.append(self.modelFactory.createCashPayment(amount))
    
    def getShelf(self, number):
        return self.getShelvesByNumber()[number]
    
    def getPendingAmount(self):
        return reduce(lambda x, y: x.amount + y.amount, self.pendingCashPayments)
    
    def completeSale(self, shelf):
        self.salesTransactions.append(self.modelFactory.createSalesTransaction(shelf.product.code, 
                                                                               shelf.product.price, 
                                                                               shelf.number, 
                                                                               self.pendingCashPayments)) 
        self.pendingCashPayments = []
    
    def returnPendingCashPayments(self):
        self.cashDispenser.dispenseCash(self.getPendingAmount())
        self.returnedCashPayments = self.returnedCashPayments + self.pendingCashPayments
        self.pendingCashPayments = []

class DeviceShelf:
    def __init__(self, itemDispenser):
        self.itemDispenser = itemDispenser
        self.number = None
        self.product = None
        self.stock = None
        self.device = None
    
    def releaseItem(self):
        if self.stock <= 0:
            raise LogicError('shelf %s is out of stock' % self.number)
        
        paidAmt = self.device.getPendingAmount()

        if paidAmt <= self.product.price:
            raise LogicError('payment of %.2f is insufficient; need %.2f' % (paidAmt, self.product.price))
        elif paidAmt == self.product.price:
            self.device.completeSale(self)
            self.itemDispenser.dispenseItem(self.number)
            self.product.decrementStock()
            self.stock = self.stock - 1
        else:
            self.device.returnPendingCashPayments()

class CashPayment:
    def __init__(self):
        self.id = None
        self.amount = None
        self.moment = None
    
    def updateAmount(self, amount):
        if amount <= 0:
            raise LogicError('amount %.2f is less than 0' % amount)
        
        self.amount = amount

class ModelFactory:
    def __init__(self, itemDispenser, cashDispenser):
        self.itemDispenser = itemDispenser
        self.cashDispenser = cashDispenser

    def createCashPayment(self, amount):
        payment = CashPayment()
        payment.id = uuid.uuid4().hex
        #payment.moment = 
        payment.updateAmount(amount)
        return payment
    
    def createSalesTransaction(self, productCode, price, shelfNumber, cashPayments):
        purchase = SalesTransaction()
        purchase.id = uuid.uuid4().hex
        #purchase.moment = 
        purchase.updateDetails(productCode, price, shelfNumber, cashPayments)
        return purchase
    
    def reCreateSalesTransaction(self, identifier, productCode, price, shelfNumber, cashPayments):
        purchase = SalesTransaction()
        purchase.id = identifier
        purchase.productCode = productCode
        purchase.shelfNumber = shelfNumber
        purchase.cashPayments = cashPayments
        #purchase.moment = 
        purchase.price = price
        return purchase
    
    def reCreateCashPayment(self, identifier, amount):
        payment = CashPayment()
        payment.id = identifier
        payment.amount = amount
        #payment.moment = 
        return payment
    
    def reCreateDeviceShelf(self, number, product, stock):
        shelf = DeviceShelf(self.itemDispenser)
        shelf.number = number
        shelf.product = product
        shelf.stock = stock
        return shelf
    
    def reCreateDevice(self, shelves, returnedCashPayments, pendingCashPayments, salesTransactions):
        device = Device(self, self.cashDispenser)
        device.salesTransactions = salesTransactions
        device.shelves = shelves
        device.returnedCashPayments = returnedCashPayments
        device.pendingCashPayments = pendingCashPayments
        return device
    
    def reCreateProduct(self, identifier, price, code, stock):
        product = Product()
        product.id = identifier
        product.price = price
        product.stock = stock
        product.code = code
        return product
