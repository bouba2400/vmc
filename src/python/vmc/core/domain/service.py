
class AcceptCashPaymentTask:
    def __init__(self, repository):
        self.repository = repository
        self.device = None
    
    def execute(self, amount):
        device = self.device if self.device else self.repository.getDevice()
        device.acceptCashPayment(amount)
        self.repository.updateDevice(device)
        self.device = device

class ReleaseShelfItemTask:
    def __init__(self, repository):
        self.repository = repository
        self.device = None
    
    def execute(self, number):
        device = self.device if self.device else self.repository.getDevice()
        device.getShelf(number).releaseItem()
        self.repository.updateDevice(device, True)
        self.device = device

class ReturnCashPaymentsTask:
    def __init__(self, repository):
        self.repository = repository
        self.device = None
    
    def execute(self):
        device = self.device if self.device else self.repository.getDevice()
        device.returnPendingCashPayments()
        self.repository.updateDevice(device)
        self.device = device
