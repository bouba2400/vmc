import json
from vmc.core.domain.repository import EntityRepository

class LocalStorageProvider(EntityRepository):
    def __init__(self, options, modelFactory):
        self.options = options
        self.modelFactory = modelFactory
        self.data = None

    def prep(self):
        path = self.options.get('device.localstorage.main.filepath')

        with open(path) as f:
            self.data = json.load(f)

    def getDevice(self):
        returnedCashPayments = \
            map(lambda x: self.modelFactory.reCreateCashPayment(x['id'], x['amount']), self.data['returnedCashPayments'])
        pendingCashPayments = \
            map(lambda x: self.modelFactory.reCreateCashPayment(x['id'], x['amount']), self.data['pendingCashPayments'])
        salesTransactions = \
            [self.modelFactory.reCreateSalesTransaction(x['productCode'], 
                                                        x['price'], x['shelfNumber'],
                                                        map(lambda y: self.modelFactory.reCreateCashPayment(y['id'], 
                                                                                                            y['amount']), 
                                                            x['cashPayments'])) 
             for x in self.data['salesTransactions']]
        shelves = map(lambda x: 
                      self.modelFactory.reCreateDeviceShelf(x['number'], 
                                                            self.modelFactory.reCreateProduct(x['product'], 
                                                                                              self.data['products'][x['product']]['price'], 
                                                                                              self.data['products'][x['product']]['code'], 
                                                                                              self.data['products'][x['product']]['stock']), 
                                                            x['stock']), 
                      self.data['shelves'])
        device = self.modelFactory.reCreateDevice(shelves, 
                                                  returnedCashPayments, 
                                                  pendingCashPayments, 
                                                  salesTransactions)

        return device

    def updateDevice(self, device, deep=True):
        raise NotImplementedError
