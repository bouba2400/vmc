#!/usr/bin/python
# -*- coding: utf-8 -*-
from setuptools import setup, find_packages

setup(
    name='vmc',
    version='1.0.0',
    author='B.D.',
    description='TBU',
    package_dir={'': 'src/python'},
    packages=find_packages(),
    install_requires=[],
    entry_points={'console_scripts': ['vmc-tradingctl=vmc.salesservice.cmd:main',
                                      'vmc-admin=vmc.operatortextui.cmd:main']},
    )
