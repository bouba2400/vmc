#!/bin/bash

case "$1" in
	upload)
		apt-get update && apt-get install -y zip
		mkdir /tmp/appbundle
		cp build/dist/* /tmp/appbundle
		cd /tmp/appbundle
		zip appbundle.zip *
		git clone git@bitbucket.org:bouba2400/vmc-builds.git /tmp/app-builds
		cd /tmp/app-builds
		git config user.email "bitbucket-pipelines-runner@vmc.test"
		git config user.name "bitbucket-pipelines-runner"
		git checkout $2
		cp /tmp/appbundle/appbundle.zip appbundle.zip
		echo $BITBUCKET_COMMIT > appbundle.commit.txt
		git add appbundle.zip appbundle.commit.txt
		git commit -m "$3"
		git push origin $2
		;;
		
	*)
		echo $"Usage: $0 {upload}"
		exit 1
 
esac